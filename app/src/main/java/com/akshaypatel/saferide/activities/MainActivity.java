package com.akshaypatel.saferide.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.akshaypatel.saferide.R;
import com.akshaypatel.saferide.fragments.FragmentAddContacts;
import com.akshaypatel.saferide.fragments.FragmentMainMenu;
import com.akshaypatel.saferide.fragments.FragmentSafestRoute;

public class MainActivity extends AppCompatActivity  {
public static  String FRAGMENT_NAME="FRAGMENT_NAME";


public   static int CHOOSE_SOURCE_DESTINATION=5;
    public static String FRAGMENT_SAFEST_ROUTE="FRAGMENT_SAFEST_ROUTE";
    public static String FRAGMENT_ADD_CONTACTS="FRAGMENT_ADD_CONTACTS";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                String value = extras.getString(FRAGMENT_NAME);
           //     Toast.makeText(getApplicationContext(), value, Toast.LENGTH_SHORT).show();

                //The key argument here must match that used in the other activity\
                if (value.equals(FRAGMENT_SAFEST_ROUTE)) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_layout, new FragmentSafestRoute());
                    ft.commit();
                } else if (value.equals(FRAGMENT_ADD_CONTACTS)) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_layout, new FragmentAddContacts());
                    ft.commit();
                }
                extras.clear();
            } else {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                ft.replace(R.id.frame_layout, new FragmentMainMenu());
                ft.commit();
            }
        }catch (NullPointerException e)
        {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            ft.replace(R.id.frame_layout, new FragmentMainMenu());
            ft.commit();
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }



}
