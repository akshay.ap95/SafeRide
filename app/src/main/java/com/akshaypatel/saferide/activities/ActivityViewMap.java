package com.akshaypatel.saferide.activities;

/**
 * Created by row_hammer on 17/2/17.
 *
 */

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.akshaypatel.saferide.DataParser;
import com.akshaypatel.saferide.GetStreetNames;
import com.akshaypatel.saferide.R;
import com.akshaypatel.saferide.StreetParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Handler;

import static com.akshaypatel.saferide.activities.MainActivity.FRAGMENT_ADD_CONTACTS;
import static com.akshaypatel.saferide.activities.MainActivity.FRAGMENT_NAME;
import static com.akshaypatel.saferide.activities.MainActivity.FRAGMENT_SAFEST_ROUTE;
import static com.akshaypatel.saferide.activities.ServerUrlSetter.SAFE_RIDE_SERVER_URL;
import static com.akshaypatel.saferide.fragments.FragmentEditContactDetails.CONTACT1;
import static com.akshaypatel.saferide.fragments.FragmentEditContactDetails.CONTACT2;
import static com.akshaypatel.saferide.fragments.FragmentEditContactDetails.CONTACT3;
import static com.akshaypatel.saferide.fragments.FragmentEditContactDetails.SAFE_RIDE_CONTACTS;
import static com.akshaypatel.saferide.fragments.FragmentSafestRoute.RESULT_INTENT_DESTINATION;
import static com.akshaypatel.saferide.fragments.FragmentSafestRoute.RESULT_INTENT_GENDER;
import static com.akshaypatel.saferide.fragments.FragmentSafestRoute.RESULT_INTENT_SOURCE;
import static com.akshaypatel.saferide.fragments.FragmentSafestRoute.RESULT_INTENT_TIME;

public class ActivityViewMap extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,View.OnClickListener, View.OnLongClickListener {
    private FloatingActionButton floatingActionButtonAllPaths, floatingActionButtonNotificataion;
    private Button buttonFindRoute;
    PolylineOptions lineOptionsarray[]=new PolylineOptions[3];
    public static final String BROADCAST_FILTER = "ManageConection_broadcast_receiver_intent_filter";

    private static int  CHOOSE_SORUCE_DESTINATION=5;
    private static int  CHOOSE_CONTACTS=6;
    private static int  CHOOSE_ROUTE=5;
    private  String GENDER="FEMALE";
    private String TIMING="DUSK";
    PopupWindow popUpWindow;
    AppBarLayout.LayoutParams layoutParams;
    Button btnClickHere;
    LinearLayout containerLayout;
    TextView tvMsg;
    private static int DEFAULT_ZOOM_LEVEL=10;
    public static String SERVER_URL;

    List<List<HashMap<String, String>>> resultRoutes;

    private static String TAG = "===ActivityViewMap";
    private GoogleMap mMap;
    ArrayList<LatLng> MarkerPoints;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
 //   LocationRequest mLocationRequest;
    DataParser parser;
    StreetParser streetParser;
    List<List<HashMap<String, String>>> LatLngArr;
    private static int TYPE_OF_FETCH = 0;
    BroadcastReceiver mReceiver;


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mReceiver, new IntentFilter(BROADCAST_FILTER));
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_map);

        SharedPreferences sharedPreferences=getSharedPreferences(SAFE_RIDE_SERVER_URL, Context.MODE_PRIVATE);
       SERVER_URL= sharedPreferences.getString(SAFE_RIDE_SERVER_URL,null);



        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Do what you need in here
                try {
                    int val;
                    val= Integer.parseInt(intent.getStringExtra("connection_established"));
                    mMap.addPolyline(lineOptionsarray[val].color(Color.GREEN));

                }catch (NumberFormatException e)
                {
                    Toast.makeText(getApplicationContext(),"Server Error",Toast.LENGTH_SHORT).show();
                }
                Log.i(TAG,intent.getStringExtra("connection_established"));

            /*   for(int i=0;i<lineOptionsarray.length;i++)
               {
                   lineOptionsarray

               }*/
            }
        };


        floatingActionButtonAllPaths = (FloatingActionButton) findViewById(R.id.floatingActionButton_all_routes);
        floatingActionButtonAllPaths.setOnLongClickListener(this);

        floatingActionButtonNotificataion = (FloatingActionButton) findViewById(R.id.floatingActionButton_notification);
        floatingActionButtonNotificataion.setOnLongClickListener(this);
        floatingActionButtonNotificataion.setOnClickListener(this);
        floatingActionButtonAllPaths.setOnClickListener(this);
        buttonFindRoute=(Button)findViewById(R.id.button_find_route);
        buttonFindRoute.setOnClickListener(this);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Initializing
        MarkerPoints = new ArrayList<>();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

        // Setting onclick event listener for the map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {

                // Already two locations
                if (MarkerPoints.size() > 1) {
                    MarkerPoints.clear();
                    mMap.clear();
                }

                // Adding new item to the ArrayList
                MarkerPoints.add(point);

                // Creating MarkerOptions
                MarkerOptions options = new MarkerOptions();

                // Setting the position of the marker
                options.position(point);

                /**
                 * For the start location, the color of marker is GREEN and
                 * for the end location, the color of marker is RED.
                 */
                if (MarkerPoints.size() == 1) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else if (MarkerPoints.size() == 2) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }


                // Add new marker to the Google Map Android API V2
                mMap.addMarker(options);

                // Checks, whether start and end locations are captured
                if (MarkerPoints.size() >= 2) {
                    LatLng origin = MarkerPoints.get(0);
                    LatLng dest = MarkerPoints.get(1);

                    // Getting URL to the Google Directions API
                    String url = getUrl(origin, dest);
                //    Log.d("onMapClick", url.toString());
                    FetchUrl FetchUrl = new FetchUrl();
                    TYPE_OF_FETCH = 1;
                    // Start downloading json data from Google Directions API
                    FetchUrl.execute(url);
                    //move map camera
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM_LEVEL));
                }

            }
        });

    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        String alternatives = "alternatives=true";
        // Sensor enabled
        // String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest /*+"&" +*//* sensor */ + "&" + alternatives;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            //Log.d("===downloadUrl", data.toString());
            //Log.i(TAG, data);

            urlConnection.disconnect();
        }
        return data;
    }


    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";
            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
               // Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (TYPE_OF_FETCH == 1) {
                ParserTask parserTask = new ParserTask();
                // Invokes the thread for parsing the JSON data
                parserTask.execute(result);
            } else if (TYPE_OF_FETCH == 2) {
              //  Log.i(TAG, "GOT STREET" + result);
                try {
                    JSONObject jObject = new JSONObject(result);
                    streetParser=new StreetParser();
                    streetParser.parseStreet(jObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {

                    jObject = new JSONObject(jsonData[0]);
                  //  Log.d(TAG, "ParserTask" + jsonData[0].toString());
                    parser = new DataParser();
                  //  Log.d(TAG, "ParserTask" + parser.toString());

                    // Starts parsing data
                    routes = parser.parse(jObject);
                 //   Log.d(TAG, "ParserTask" + "Executing routes");
                //    Log.d(TAG, "ParserTask" + routes.toString());
                    LatLngArr=routes;
                //    Log.d(TAG, "ParserTask" + routes.getClass().getName());



            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;
           // Log.i(TAG, String.valueOf(result.size()));
           // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();
                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);

                if (i == 0)
                    lineOptions.color(Color.BLUE);
                if (i == 1)
                    lineOptions.color(Color.BLACK);
                if (i == 2) lineOptions.color(Color.YELLOW);

              //  Log.d("====onPostExecute", "onPostExecute lineoptions decoded");
                lineOptionsarray[i] = lineOptions;
                // Drawing polyline in the Google Map for the i-th route
                if (lineOptions != null) {
                   // mMap.addPolyline(lineOptions);

                } else {
                    Log.d("===onPostExecute", "without Polylines drawn");
                }
            }


        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

     /*  mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }*/

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floatingActionButton_all_routes:

                try{

                    // StreetNames streetNames=new StreetNames();
                    GetStreetNames downloadFilesTask=new GetStreetNames(getApplicationContext(),parser.getStepCoordinates(),parser.getStepDistances(),GENDER,TIMING);
                    downloadFilesTask.execute();
                    //   streetNames.downloadAllNames(parser.getStepCoordinates(),parser.getStepDistances());
                    //    streetNames.getStreetNames(parser.getStepCoordinates(),parser.getStepDistances());
                    parser.getStepCoordinates();
                }catch (NullPointerException e)
                {
                    Toast.makeText(this,"Please choose source and destination",Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.floatingActionButton_notification:
                //Toast.makeText(this, "MANAGE CONTACTS", Toast.LENGTH_SHORT).show();
                Intent x =new Intent(getApplicationContext(),MainActivity.class);
                x.putExtra(FRAGMENT_NAME,FRAGMENT_ADD_CONTACTS);
                startActivityForResult(x,CHOOSE_SORUCE_DESTINATION);
                break;
            case  R.id.button_find_route:

                Intent i3 =new Intent(getApplicationContext(),MainActivity.class);
                i3.putExtra(FRAGMENT_NAME,FRAGMENT_SAFEST_ROUTE);
                startActivityForResult(i3,CHOOSE_ROUTE);
                break;

        }
    }

//
    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.floatingActionButton_notification:
                sendSMS();
                return true;
            case R.id.floatingActionButton_all_routes:
                for(int i=0;i<lineOptionsarray.length;i++)
               {
                   if(lineOptionsarray[i]!=null)
                   {
                       mMap.addPolyline(lineOptionsarray[i]);
                   }

               }
                return  true;
        }
        return false;
    }


    /**
     * Use this for managinf the result obtained from choosing path code =5
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CHOOSE_SORUCE_DESTINATION) {
            if(resultCode==RESULT_OK) {
               // Toast.makeText(this, "Choosen TRAVEL", Toast.LENGTH_SHORT).show();
               LatLng source= data.getParcelableExtra(RESULT_INTENT_SOURCE);
                LatLng destination=data.getParcelableExtra(RESULT_INTENT_DESTINATION);
                GENDER=data.getStringExtra(RESULT_INTENT_GENDER);
                 TIMING=data.getStringExtra(RESULT_INTENT_TIME);
                MarkerPoints.clear();
                mMap.clear();
                MarkerOptions options = new MarkerOptions();
                options.position(source);
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                MarkerPoints.add(source);
                mMap.addMarker(options);
                options.position(destination);
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                mMap.addMarker(options);
                MarkerPoints.add(destination);

                String url = getUrl(source, destination);
                //    Log.d("onMapClick", url.toString());
                FetchUrl FetchUrl = new FetchUrl();
                TYPE_OF_FETCH = 1;
                // Start downloading json data from Google Directions API
                // Adding new item to the ArrayList
               FetchUrl.execute(url);
                //move map camera
                mMap.moveCamera(CameraUpdateFactory.newLatLng(source));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM_LEVEL));


            }//[END RESULT OK FOR CHOOSE SOU]
       }
        else if(requestCode==CHOOSE_CONTACTS)
        {
            if(requestCode==RESULT_OK)
            Toast.makeText(this, "CONTACTS UPDATED", Toast.LENGTH_SHORT).show();
        }
        else if(requestCode==CHOOSE_ROUTE)
        {
            if(requestCode==RESULT_OK)
            {
               // Toast.makeText(this, "ROUTE CHOSEN", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private  void sendSMS()
    {
        SmsManager smsManager = SmsManager.getDefault();
        SharedPreferences sharedPreferences=getSharedPreferences(SAFE_RIDE_CONTACTS,Context.MODE_PRIVATE);
        Set<String> c1=  sharedPreferences.getStringSet(CONTACT1,null);
        Set<String> c2=  sharedPreferences.getStringSet(CONTACT2,null);
        Set<String> c3=  sharedPreferences.getStringSet(CONTACT3,null);
        if(c1==null&&c2==null&&c3==null)
        {
            Toast.makeText(getApplicationContext(),"No contacts present",Toast.LENGTH_SHORT).show();

        }
        if(c1!=null)
        {
            smsManager.sendTextMessage(String.valueOf(c1.toArray()[0]), null, "Need Help!", null, null);
            Log.i(TAG,"C1 number : " + (CharSequence)c1.toArray()[0]);

        }
      /*  if(c2!=null)
        {
            smsManager.sendTextMessage(String.valueOf(c2.toArray()[0]), null, "Help!", null, null);
            Log.i(TAG,"C2 number : " + (CharSequence)c2.toArray()[0]);
        }
        if(c3!=null)
        {
            smsManager.sendTextMessage(String.valueOf(c3.toArray()[0]), null, "Help! ", null, null);
            Log.i(TAG,"C3 number : " + (CharSequence)c3.toArray()[0]);

        }
*/
    }

}//[END CLASS MAIN ACTIVITY]