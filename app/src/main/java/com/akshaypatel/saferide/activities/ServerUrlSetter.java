package com.akshaypatel.saferide.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.akshaypatel.saferide.R;

import static com.akshaypatel.saferide.fragments.FragmentEditContactDetails.SAFE_RIDE_CONTACTS;

public class ServerUrlSetter extends AppCompatActivity {
        EditText editTextServerUrl;
    public static  String SAFE_RIDE_SERVER_URL="";
    Button buttonSaveUrl;
    private static String TAG="===ServerUrlSetter";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_url_setter);
        editTextServerUrl=(EditText)findViewById(R.id.editText_serverUrl);
        buttonSaveUrl=(Button)findViewById(R.id.button_save_server_url);


        SharedPreferences sharedPreferences=getSharedPreferences(SAFE_RIDE_SERVER_URL, Context.MODE_PRIVATE);
        String serverUrl=sharedPreferences.getString(SAFE_RIDE_SERVER_URL,null);
        if(serverUrl!=null)
        {
            editTextServerUrl.setText(serverUrl);

        }



        buttonSaveUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences=getSharedPreferences(SAFE_RIDE_SERVER_URL, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString(SAFE_RIDE_SERVER_URL,editTextServerUrl.getText().toString());
                editor.apply();
                Log.i(TAG,"SERVER URL"+ editTextServerUrl.getText().toString());



            }
        });


    }
}
