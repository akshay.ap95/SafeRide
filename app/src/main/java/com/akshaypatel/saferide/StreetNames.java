package com.akshaypatel.saferide;

import android.nfc.Tag;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LongSparseArray;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by row_hammer on 4/3/17.
 *
 */

public class StreetNames {
private StreetParser streetParser;
private static  String TAG="===StreetName";
    private static int JUMPS=5;
    /*
     *
     *
     * A method to download json data from url
     */

    ArrayList<Wrapper> arrayWrapper=new ArrayList<>();
    ArrayList<Wrapper> arrayWrapper1=new ArrayList<>();


    public void getStreetName(String Lat, String Lng) {

        String Url = "https://maps.googleapis.com/maps/api/geocode/json?latlng"+"="+Lat+ ","+Lng+"\n";
        FetchUrl FetchUrl= new FetchUrl(false);
        Wrapper wrapper=new Wrapper();
        wrapper.lat=Lat;
        wrapper.lng=Lng;
        wrapper.url=Url;
        FetchUrl.execute(wrapper);
    }




    public void getStreetNames( List<List<HashMap<String, String>>> LatLngArr,List<List<String>> stepDistances) {
     //   Log.i(TAG,"Number of Routes"+String.valueOf(LatLngArr.size()));
      //  Log.i(TAG,"Length of stepDistances arr"+String.valueOf(stepDistances.size()));

        for(int i=0;i<LatLngArr.size();i++)
        {
            List<String> distanceForithRoute=stepDistances.get(i);
            List<HashMap<String,String>> roue=LatLngArr.get(i);
         //   Log.i(TAG,"Number of Co-ordinates in route: "+i+" :"+String.valueOf(roue.size()));

            for(int j =0;j<roue.size()%JUMPS+1;j=j+1)
            {

                HashMap<String, String> point = roue.get(j);
                String lat =point.get("lat");
                String lng =point.get("lng");
                String Url = "https://maps.googleapis.com/maps/api/geocode/json?latlng"+"="+lat+ ","+lng+"\n";
                FetchUrl FetchUrl1;
               if((i==LatLngArr.size()-1)&&(j==roue.size()-1))
               { FetchUrl1= new FetchUrl(true);}
                else
               {FetchUrl1 = new FetchUrl(false);}
                Wrapper wrapper=new Wrapper();
                wrapper.lat=lat;
                wrapper.lng=lng;
                Log.i(TAG," j is "+j+ " "+distanceForithRoute.size());
                try {
                    wrapper.streetLength=distanceForithRoute.get(j);
                } catch (IndexOutOfBoundsException IOB)
                {
                    Log.i(TAG,"INDEX OUT OF BOUNDS");
                }
                wrapper.routeNumber=i+1;
                wrapper.url=Url;
                FetchUrl1.execute(wrapper);
           }
        }
   }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
        //    Log.d("===downloadUrl", data.toString());
          //  Log.i(TAG, data);

            urlConnection.disconnect();
        }
        return data;
    }
    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<Wrapper, Void, Wrapper> {
        boolean isItLast=false;
        public FetchUrl(boolean isItLast)
        {
this.isItLast=isItLast;
       }
        @Override
        protected Wrapper doInBackground(Wrapper... wrappers) {
            // For storing data from web service
            try {
                // Fetching the data from web service
                /*
               * Call Download URL to fetch the data
                * */
                wrappers[0].response = downloadUrl(wrappers[0].url);
             //   Log.i(TAG,"LAT TO FETCH" +wrappers[0].lat+" ___"+wrappers[0].lng);
             //   Log.d("Background Task data", wrappers[0].response.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return wrappers[0];
        }
        @Override
        protected void onPostExecute(Wrapper wrapper) {
            super.onPostExecute(wrapper);
                //  Log.i(TAG, "GOT STREET" + result);
                try {
                    if (wrapper.response!=null)
                    { JSONObject jObject = new JSONObject(wrapper.response);
                    streetParser=new StreetParser();
                    streetParser.parseStreet(jObject);
                    wrapper.streetName=streetParser.getStreetName();
                    wrapper.streetNumber=streetParser.getStreetNumber();
                    arrayWrapper.add(wrapper);
                    Log.i(TAG,wrapper.routeNumber+" "+wrapper.lat+" "+wrapper.lng+" "+wrapper.streetName+" "+ wrapper.streetLength);}
                    if(isItLast==true)
                    {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
    }
}//[END STREET NAMES]
