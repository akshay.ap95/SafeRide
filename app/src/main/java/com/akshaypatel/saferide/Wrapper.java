package com.akshaypatel.saferide;

import android.util.Log;

/**
 * Created by row_hammer on 22/3/17.
 */
public class Wrapper {
    public int routeNumber;
    public String lat;
    public String url;
    public String lng;
    public String response;
    public String gender;
    public String timing;
    public String streetName=" ";
    public String streetLength;
    public String streetNumber;
    private static String TAG = "==Wrapper";

        public long convertStreetLength(String streetLength) {
        long result;
        String arr[] = streetLength.split(" ");
        if (arr[1].equals("km")) {
            result = Long.parseLong(arr[0]) * 1000;
            Log.i(TAG, "Retruning " + result);
            return result;
        } else if (arr[1].equals("m")) {
            result = (long) (Long.parseLong(arr[0])) ;
            Log.i(TAG, "Retruning " + result);
            return result;

        } else if (arr[1].equals("mi")) {
            result = (long) (Long.parseLong(arr[0]) * 1609.34);
            Log.i(TAG, "Retruning " + result);
            return result;

        } else if (arr[1].equals("ft")) {
            result = (long) (Long.parseLong(arr[0]) * 0.3048);
            Log.i(TAG, "Retruning " + result);
            return result;

        }

        return (long) 0.300;
    }
}
