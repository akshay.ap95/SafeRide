package com.akshaypatel.saferide.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.akshaypatel.saferide.R;
import com.akshaypatel.saferide.activities.ActivityViewMap;
import com.akshaypatel.saferide.activities.ServerUrlSetter;


public class FragmentMainMenu extends Fragment implements View.OnClickListener, View.OnLongClickListener {
    Context ctx;
    View V;
    ImageButton b1,b2,b3;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        V=inflater.inflate(R.layout.fragment_main_menu,container,false);
        b1=(ImageButton)V.findViewById(R.id.button_share_journey);
        b1.setOnClickListener(this);
        b2=(ImageButton)V.findViewById(R.id.button_safest_route);
        b2.setOnLongClickListener(this);
        b2.setOnClickListener(this);
        b3=(ImageButton)V.findViewById(R.id.button_contacts);
        b3.setOnClickListener(this);
        return V;
       }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_contacts:

                FragmentTransaction ftAddContacts = getActivity().getSupportFragmentManager().beginTransaction();
                // ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                FragmentAddContacts fragmentAddContacts=new FragmentAddContacts();
                ftAddContacts.replace(this.getId(),fragmentAddContacts);
                ftAddContacts.addToBackStack("tag");
                ftAddContacts.commit();
                Log.i("===FragmentMainMenu","After Commit safest route transaction");

                break;
            case R.id.button_safest_route:
                Log.i("===FragmentMainMenu","Click Safest Route");

                Intent i =new Intent(getContext(),ActivityViewMap.class);
                startActivity(i);
               // FragmentTransaction ft_safest_route = getActivity().getSupportFragmentManager().beginTransaction();
                // ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
              /*  FragmentSafestRoute fragmentSafestRoute=new FragmentSafestRoute();
                ft_safest_route.replace(this.getId(),fragmentSafestRoute);
                ft_safest_route.addToBackStack("tag");
                ft_safest_route.commit();*/
                Log.i("===FragmentMainMenu","After Commit safest route transaction");

                break;
            case R.id.button_share_journey:
                Log.i("===FragmentMainMenu","Click Share journey");
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
               // ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                FragmentVerifiedUsers fragmentVerifiedUsers=new FragmentVerifiedUsers();
                ft.replace(this.getId(),fragmentVerifiedUsers);
                ft.addToBackStack("tag");
                ft.commit();
                Log.i("===FragmentMainMenu","After Commit Share journey transaction");

                break;


        }
    }//on click

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()){
            case R.id.button_safest_route:
                    Intent i=new Intent(getContext(), ServerUrlSetter.class);
                    startActivity(i);
                    return true;
            default:
                break;
        }
        return  true;
    }
}