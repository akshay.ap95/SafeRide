package com.akshaypatel.saferide.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.support.v4.app.Fragment;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.akshaypatel.saferide.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.google.android.gms.wearable.DataMap.TAG;

/**
 * Created by row_hammer on 23/2/17.
 *
 */

public class FragmentSafestRoute extends Fragment implements View.OnClickListener{
    Context ctx;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE_SOURCE = 2;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE_DESTIANTION= 3;

    public static String RESULT_INTENT_SOURCE="RESULT_INTENT_SOURCE";
    public static String RESULT_INTENT_DESTINATION="RESULT_INTENT_DESTINATION";
    public static String RESULT_INTENT_GENDER="RESULT_INTENT_GENDER";
    public static String RESULT_INTENT_GENDER_MALE="MALE";
    public static String RESULT_INTENT_GENDER_FEMALE="FEMALE";

    public static String RESULT_INTENT_TIME="RESULT_INTENT_TIME";
    public static String RESULT_INTENT_TRAVEL_TIME="DUSK";


    LatLng source,destination;
    RadioGroup radioGroupGender;
    TimePicker timePickerTravelTime;


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

       /*     if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(getContext(), data);
                    Log.i(TAG, "Place: " + place.getName());
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(getContext(), data);
                    // TODO: Handle the error.
                    Log.i(TAG, status.getStatusMessage());

                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                }

        }*/
    }

    View V;
    ImageButton b1,b2;
    PlaceAutocompleteFragment autocompleteFragmentSource;
    PlaceAutocompleteFragment autocompleteFragmentDestination;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        V=inflater.inflate(R.layout.fragment_safest_route,container,false);
        b1=(ImageButton)V.findViewById(R.id.image_button_verified_user_ok);
        b2=(ImageButton)V.findViewById(R.id.image_button_verified_user_current_location);
        b2.setOnClickListener(this);
        b1.setOnClickListener(this);


        timePickerTravelTime=(TimePicker)V.findViewById(R.id.timePicker_travel_time);
        timePickerTravelTime.setIs24HourView(true);

        radioGroupGender=(RadioGroup)V.findViewById(R.id.radio_group_gender);

        autocompleteFragmentSource    = (PlaceAutocompleteFragment)
                getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_source);


        autocompleteFragmentDestination  = (PlaceAutocompleteFragment)
                getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_destination);



        SetPlaceChooserListener();
        return V;
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.image_button_verified_user_ok:
                Intent resultIntent = new Intent();
                if(source==null)
                {
                    Toast.makeText(getContext(),"PLEASE CHOOSE SOURCE",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(destination==null)
                {
                    Toast.makeText(getContext(),"PLEASE CHOOSE DESTINATION",Toast.LENGTH_SHORT).show();

                    return;
                }

                if((radioGroupGender.getCheckedRadioButtonId()==-1))
                {
                    Toast.makeText(getContext(),"PLEASE CHOOSE GENDER",Toast.LENGTH_SHORT).show();

                    return;
                }
                else {
                    int selectedId = radioGroupGender.getCheckedRadioButtonId();
                    if (selectedId ==( (RadioButton) V.findViewById(R.id.radio_button_gender_male)).getId())
                    {
                        resultIntent.putExtra(RESULT_INTENT_GENDER,RESULT_INTENT_GENDER_MALE);

                    }
                    else  if(selectedId ==( (RadioButton) V.findViewById(R.id.radio_button_gender_female)).getId())

                    {
                        resultIntent.putExtra(RESULT_INTENT_GENDER,RESULT_INTENT_GENDER_FEMALE);

                    }

                }
                int hour=16,minute=16;
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
                   hour= timePickerTravelTime.getCurrentHour();
                     minute = timePickerTravelTime.getCurrentMinute();
                }else
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                         hour = timePickerTravelTime.getHour();
                         minute = timePickerTravelTime.getMinute();
                    }

                if(hour<20&&hour>=18)
                    RESULT_INTENT_TRAVEL_TIME="DUSK";
                else if(hour>=20&&hour<3)
                    RESULT_INTENT_TRAVEL_TIME="NIGHT";
                else if(hour>=3&&hour<12)
                    RESULT_INTENT_TRAVEL_TIME="MORNING";
                else if(hour>=12&&hour<18)
                    RESULT_INTENT_TRAVEL_TIME="NOON";


                resultIntent.putExtra(RESULT_INTENT_SOURCE,source);
                resultIntent.putExtra(RESULT_INTENT_DESTINATION,destination);
                resultIntent.putExtra(RESULT_INTENT_TIME,RESULT_INTENT_TRAVEL_TIME);

                getActivity().setResult(RESULT_OK, resultIntent);
                getActivity().finish();

                break;
            /*case  R.id.image_button_verified_user_current_location:
                break;*/

        }
    }//on click

    private void SetPlaceChooserListener()
    {

        autocompleteFragmentSource.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                source=place.getLatLng();


            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });

        autocompleteFragmentDestination.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                destination=place.getLatLng();

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });
    }

}

