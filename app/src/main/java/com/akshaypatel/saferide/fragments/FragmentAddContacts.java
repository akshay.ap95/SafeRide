package com.akshaypatel.saferide.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.akshaypatel.saferide.R;

import java.util.Set;

import static com.akshaypatel.saferide.fragments.FragmentEditContactDetails.CONTACT1;
import static com.akshaypatel.saferide.fragments.FragmentEditContactDetails.CONTACT2;
import static com.akshaypatel.saferide.fragments.FragmentEditContactDetails.CONTACT3;
import static com.akshaypatel.saferide.fragments.FragmentEditContactDetails.SAFE_RIDE_CONTACTS;

/**
 * Created by row_hammer on 23/2/17.
 *
 */

public class FragmentAddContacts extends Fragment implements View.OnClickListener{
    Context ctx;
    View V;
    private static String TAG="===FragAddContacts";
    Button b1,b2;
    TextView t1,t2,t3,n1,n2,n3;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        V=inflater.inflate(R.layout.fragment_add_contacts,container,false);

        SetUiReferences();
       // LoadContacts();

        return V;
    }

    @Override
    public void onResume() {
        LoadContacts();
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }



    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.image_button_add_contacts_add:
                Log.i(TAG,"CLICK ADD CONTACTS");
                FragmentTransaction ft_add_details = getActivity().getSupportFragmentManager().beginTransaction();
                // ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                FragmentEditContactDetails fragmentAddDetails=new FragmentEditContactDetails();
                ft_add_details.replace(this.getId(),fragmentAddDetails);
                ft_add_details.addToBackStack("tag");
                ft_add_details.commit();

                break;
            case R.id.image_button_add_contacts_clear:
                SharedPreferences sharedPreferences=getActivity().getSharedPreferences(SAFE_RIDE_CONTACTS,Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.clear();
                editor.commit();
                LoadContacts();
        }
    }//on click


    public void LoadContacts()
    {
        //Load the previous contacts if present
        SharedPreferences sharedPreferences=getActivity().getSharedPreferences(SAFE_RIDE_CONTACTS,Context.MODE_PRIVATE);
        Set<String> c1=  sharedPreferences.getStringSet(CONTACT1,null);
        Set<String> c2=  sharedPreferences.getStringSet(CONTACT2,null);
        Set<String> c3=  sharedPreferences.getStringSet(CONTACT3,null);


        if(c1!=null)
        {
            t1.setText((CharSequence) c1.toArray()[1]);
            n1.setText((CharSequence) c1.toArray()[0]);

            Log.i(TAG,"C1 name : " + (CharSequence)c1.toArray()[1]);
            Log.i(TAG,"C1 number : " + (CharSequence)c1.toArray()[0]);

        }
        else
        {
            t1.setText("No contact present");
            n1.setText(" ");

        }
        if(c2!=null)
        {
            t2.setText((CharSequence) c2.toArray()[1]);
            n2.setText((CharSequence) c2.toArray()[0]);
            Log.i(TAG,"C2 name : " + (CharSequence)c2.toArray()[1]);
            Log.i(TAG,"C2 number : " + (CharSequence)c2.toArray()[0]);
        }
        else
        {

            t2.setText("No contact present");
            n2.setText(" ");
        }

        if(c3!=null)
        {
            t3.setText((CharSequence) c3.toArray()[1]);
            n3.setText((CharSequence) c3.toArray()[0]);
            Log.i(TAG,"C3 name : " + (CharSequence)c3.toArray()[1]);
            Log.i(TAG,"C3 number : " + (CharSequence)c3.toArray()[0]);

        } else
        {

            t3.setText("No contact present");
            n3.setText(" ");
        }




    }

    private  void SetUiReferences()
    {

        t1=(TextView)V.findViewById(R.id.textView_contact1);
        n1=(TextView)V.findViewById(R.id.textView_contact_number1);


        t2=(TextView)V.findViewById(R.id.textView_contact2);
        n2=(TextView)V.findViewById(R.id.textView_contact_number2);


        t3=(TextView)V.findViewById(R.id.textView_contact3);
        n3=(TextView)V.findViewById(R.id.textView_contact_number3);

        b1=(Button)V.findViewById(R.id.image_button_add_contacts_add);
        b2=(Button)V.findViewById(R.id.image_button_add_contacts_clear);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);

    }

}

