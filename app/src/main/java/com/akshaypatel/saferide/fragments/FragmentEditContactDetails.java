package com.akshaypatel.saferide.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.akshaypatel.saferide.R;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static android.app.Activity.RESULT_OK;

/**
 * Created by row_hammer on 23/2/17.
 *
 */

public class FragmentEditContactDetails extends Fragment implements View.OnClickListener{
    Context ctx;

    public  int PICK_CONTACT = 2015;
    private Button i1,i2,i3,i4;
    public static String SAFE_RIDE_CONTACTS="SAFE_RIDE_CONTACTS";
    public static String CONTACT1="CONTACT1";
    public static String CONTACT2="CONTACT2";
    public static String CONTACT3="CONTACT3";

    private String cn1,cn2,cn3,cc1,cc2,c3;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    View V;
    private static String TAG="===FragContactsDetails";
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        V=inflater.inflate(R.layout.fragment_edit_contact_details,container,false);
        i1=(Button) V.findViewById(R.id.image_button_add_contact_details_ok);
        i1.setOnClickListener(this);

        i2=(Button) V.findViewById(R.id.image_button_contact1);
        i2.setOnClickListener(this);

        i3=(Button)V.findViewById(R.id.image_button_contact2);
        i3.setOnClickListener(this);

        i4=(Button) V.findViewById(R.id.image_button_contact3);
        i4.setOnClickListener(this);


         sharedPreferences=getActivity().getSharedPreferences(SAFE_RIDE_CONTACTS,Context.MODE_PRIVATE);
         editor=sharedPreferences.edit();

        return V;
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onClick(View view) {

        Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        
        switch (view.getId()){
            case R.id.image_button_add_contact_details_ok:
                Log.i(TAG,"CLICK SAVE BUTTON");


                editor.apply();
                Toast.makeText(getActivity().getApplicationContext(),"Changes have been saved",Toast.LENGTH_SHORT).show();

                break;
            case  R.id.image_button_contact1:
                startActivityForResult(i, PICK_CONTACT+1);
                break;
            case  R.id.image_button_contact2:
                startActivityForResult(i, PICK_CONTACT+2);
                break;
            case  R.id.image_button_contact3:
                startActivityForResult(i, PICK_CONTACT+3);
                break;
        }
    }//on click

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            Uri contactUri = data.getData();
            Cursor cursor = getActivity().getContentResolver().query(contactUri, null, null, null, null);
            cursor.moveToFirst();
            int columnName=cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int columnNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            String s1 = cursor.getString(columnNumber);
            String s2 = cursor.getString(columnName);
           String s3=s1.replace(" ","");
            String s4=s2.replace(" ","");
            String temp;
            if(isNumeric(s3)==false)
            {
                temp=s1;
                s1=s2;
                s2=temp;
            }

            String[] att = new String[]{s1, s2};
            Set<String> mySet = new HashSet<String>();
            Collections.addAll(mySet, att);
            if (resultCode == RESULT_OK) {

                Log.i(TAG,mySet.toString());

                if (requestCode == PICK_CONTACT + 1) {
                    editor.putStringSet(CONTACT1, mySet);
                } else if (requestCode == PICK_CONTACT + 2) {
                    editor.putStringSet(CONTACT2, mySet);
                } else if (requestCode == PICK_CONTACT + 3) {
                    editor.putStringSet(CONTACT3, mySet);
                }
            }

        }
        catch (NullPointerException e)
        {

            Toast.makeText(getContext(),"Contact not updated!",Toast.LENGTH_SHORT).show();

        }

    }
    public static boolean isNumeric(String str)
    {
        Log.i(TAG,"received==="+ str);

        char arr[]=str.toCharArray();
        for (int i =1;i<arr.length;i++)
        {
            Log.i(TAG,"retu");
            if (!Character.isDigit(arr[i]))
            {            Log.i(TAG,"false==="+ arr[i]);

                return false;}
        }
                    Log.i(TAG,"true==="+ str);
            return true;
    }


}

