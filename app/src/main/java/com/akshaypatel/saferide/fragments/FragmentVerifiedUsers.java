package com.akshaypatel.saferide.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.akshaypatel.saferide.R;
import com.akshaypatel.saferide.activities.ActivityViewMap;

import java.util.ArrayList;

/**
 * Created by row_hammer on 23/2/17.
 *
 */

public class FragmentVerifiedUsers extends Fragment implements View.OnClickListener{
    Context ctx;
    View V;
    private static String TAG="===FragemntVeriU";
    ImageButton b1;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        V=inflater.inflate(R.layout.fragment_verified_users,container,false);
        b1=(ImageButton)V.findViewById(R.id.image_button_verified_user_ok);
     b1.setOnClickListener(this);
        return V;
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onClick(View view) {


        //Get the List of verified users convert it to ArrayList
        ArrayList<String> arr=new ArrayList<>();
        arr.add("1");
        arr.add("2");
        arr.add("3");

        switch (view.getId()){
            case R.id.image_button_verified_user_ok:
                new MaterialDialog.Builder(getActivity())
                        .title(R.string.dialog_box_title)
                        .items(arr)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                Log.i(TAG,"Click Item "+ which);
                            }
                        })
                        .show();
                break;

        }
    }//on click

}

