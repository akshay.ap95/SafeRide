package com.akshaypatel.saferide;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static android.content.Context.VIBRATOR_SERVICE;
import static com.akshaypatel.saferide.activities.ActivityViewMap.BROADCAST_FILTER;

public class SendRequest {
    //private static String SERVER_URL = "";
    private static String TAG="===SEND REQUEST";
    private Context context;
    public SendRequest(Context context)
    {
        this.context=context;
    }
    public void getSafestRoute(String uri,String json)
    {
     //   AsyncRequest asyncRequest=new AsyncRequest(uri,json);
       // asyncRequest.execute();

        PostClass postClass=new PostClass(context,uri,json);
        postClass.execute();
    }

   /* private String makeRequest(String uri, String json) {
        HttpURLConnection urlConnection;
        String data =json;
        String result = null;
        try {
            //Connect
            urlConnection = (HttpURLConnection) ((new URL(uri).openConnection()));
             urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);

            urlConnection.connect();

            //Write
            OutputStream outputStream = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(data);
            writer.close();
            outputStream.close();

            //Read
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));

            String line;
            StringBuilder sb = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            bufferedReader.close();
            result = sb.toString();

        }  catch (IOException e) {
            e.printStackTrace();
        }
      *//*  catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*//*
        return result;
    }*/

  /*  private class AsyncRequest extends AsyncTask<String, Void, String> {

        private String uri;
        private String json;
        private AsyncRequest(String uri, String json)
        {
            this.uri=uri;
            this.json=json;

            Log.i("RECEIVED URI",this.uri);
            Log.i("RECEIVED JSON",this.json);
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
           Log.i("RECEIVED RESPONSE","__"+aVoid);
        }

        @Override
        protected String doInBackground(String... strings) {

            return  makeRequest(this.uri,this.json);
        }

    }
*/

    private class PostClass extends AsyncTask<String, Void, Void> {
        private Context context;
        private String SERVER_URL="";
        private String json="";
        private StringBuilder output;
        String OUTPUT_SAFEST_ROUTE="";
        public PostClass(Context c,String url,String json){
            final StringBuilder output ;
            this.context = c;
            this.SERVER_URL=url;
            this.json=json;
//            this.error = status;
//            this.type = t;
        }

        protected void onPreExecute(){

        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                if(SERVER_URL==null)
                {
                    Toast.makeText(context,"No server url set",Toast.LENGTH_SHORT).show();
                    return null;
                }
               URL url = new URL(SERVER_URL);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                String urlParameters = "DATA="+json;
                connection.setRequestMethod("POST");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
                connection.setDoOutput(true);
                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(urlParameters);
                dStream.flush();
                dStream.close();
                int responseCode = connection.getResponseCode();

             //   System.out.println("\nSending 'POST' request to URL : " + url);
            //    System.out.println("Post parameters : " + urlParameters);
           //     System.out.println("Response Code : " + responseCode);

                output = new StringBuilder("Request URL " + url);
                output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters);
                output.append(System.getProperty("line.separator")  + "Response Code " + responseCode);
                output.append(System.getProperty("line.separator")  + "Type " + "POST");
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                StringBuilder responseOutput = new StringBuilder();
            //    Log.i(TAG,"output===============" + br);
                while((line = br.readLine()) != null ) {
                    responseOutput.append(line);
                }
                br.close();

                output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());
              //  Log.i(TAG,"output is ===============" +responseOutput.toString());
                OUTPUT_SAFEST_ROUTE=responseOutput.toString();

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.i(TAG,"OUTPUT IS : "+OUTPUT_SAFEST_ROUTE);
            String output="";
            if(!OUTPUT_SAFEST_ROUTE.isEmpty())
            output=OUTPUT_SAFEST_ROUTE;
            Intent i = new Intent(BROADCAST_FILTER);
            i.putExtra("connection_established", output);
            context.sendBroadcast(i);


        }



    }



}

