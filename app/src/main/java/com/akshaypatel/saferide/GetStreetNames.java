package com.akshaypatel.saferide;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.akshaypatel.saferide.activities.ActivityViewMap.SERVER_URL;

/**
 *
 * Created by row_hammer on 22/3/17.
 */

//[DOWN:ALD URL CLASS]
public class GetStreetNames extends AsyncTask<Void, Integer, Void> {
   private List<List<HashMap<String, String>>> LatLngArr;
    private static String TAG="===GetStreetNames";
    private static int JUMPS=10;
    private ArrayList<Wrapper> arrayWrapper1=new ArrayList<>();
   private List<List<String>> stepDistances;
    private String GENDER,TIMING;
    private Context context;
    public GetStreetNames(Context context, List<List<HashMap<String, String>>> LatLngArr, List<List<String>> stepDistances, String GENDER, String TIMING)
    {
        this.LatLngArr=LatLngArr;
        this.stepDistances=stepDistances;
        this.GENDER=GENDER;
        this.TIMING=TIMING;
        this.context=context;
    }
    protected Void doInBackground(Void... voids) {
        downloadAllNames(this.LatLngArr,this.stepDistances, GENDER,TIMING);
        return null;

    }

    protected void onProgressUpdate(Integer... progress) {
        //  setProgressPercent(progress[0]);
    }

    protected void onPostExecute(Void result) {
        //Log.i("Downloaded " , " bytes");
        /*for(int i=0;i<arrayWrapper1.size();i++)
        {
            Wrapper w= arrayWrapper1.get(i);
         //   Log.i(TAG,"routeNUMBER : "+w.routeNumber+" StreetName : "+w.streetName+ " Length " + w.streetLength+" GENDER : "+ w.gender+" TIMING :"+w.timing);


        }*/

        JSONObject results= ConvertObjectToJson(arrayWrapper1);
    }//[END OF POST EXECUTE]




    private void downloadAllNames(List<List<HashMap<String, String>>> LatLngArr,List<List<String>> stepDistances,String GENDER,String TIMING)
    {

       Log.i(TAG,"Number of Routes"+String.valueOf(LatLngArr.size()));
      //  Log.i(TAG,"Length of stepDistances arr"+String.valueOf(stepDistances.size()));

        for(int i=0;i<LatLngArr.size();i++)
        {
            List<String> distanceForithRoute=stepDistances.get(i);
            List<HashMap<String,String>> roue=LatLngArr.get(i);
            Log.i(TAG,"Number of Co-ordinates in route: "+i+" :"+String.valueOf(roue.size()));

            for(int j =0;j<roue.size()%JUMPS;j++)
            {
               // Log.i(TAG,"SENDING ROUTE " + j);

                HashMap<String, String> point = roue.get(j);
                String lat =point.get("lat");
                String lng =point.get("lng");
            //    Log.i(TAG ,"LAT : "+ lat +" LNG : "+lng);
                String Url = "https://maps.googleapis.com/maps/api/geocode/json?latlng"+"="+lat+ ","+lng+"\n";


                Wrapper wrapper=new Wrapper();
                wrapper.lat=lat;
                wrapper.lng=lng;

            //    Log.i(TAG," j is "+j+ " "+distanceForithRoute.size());
                try {
                    wrapper.streetLength=distanceForithRoute.get(j);

                } catch (IndexOutOfBoundsException IOB)
                {
                    Log.i(TAG,"INDEX OUT OF BOUNDS");
                }

                wrapper.routeNumber=i+1;
                wrapper.url=Url;
                try {
                    wrapper.response=downloadUrl(wrapper.url);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    if (wrapper.response!=null)
                    { JSONObject jObject = new JSONObject(wrapper.response);
                     StreetParser  streetParser=new StreetParser();
                        streetParser.parseStreet(jObject);
                        wrapper.streetName=streetParser.getStreetName();
                        wrapper.streetNumber=streetParser.getStreetNumber();
                        wrapper.gender=GENDER;
                        wrapper.timing=TIMING;
                        arrayWrapper1.add(wrapper);
                       // Log.i(TAG,wrapper.routeNumber+" "+wrapper.lat+" "+wrapper.lng+" "+wrapper.streetName+" "+ wrapper.streetLength);
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

    }//[END GET ALL URL ]



    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();
        } catch (Exception e) {
            Log.d("Exception download url", e.toString());

        }
        finally {
            iStream.close();
            //    Log.d("===downloadUrl", data.toString());
            //  Log.i(TAG, data.toString());

            urlConnection.disconnect();
        }

        return data;
    }

    private JSONObject  ConvertObjectToJson(ArrayList<Wrapper> wrappers)
    {
        JSONObject jResult = new JSONObject();// main object
        JSONArray jArray = new JSONArray();// /ItemDetail jsonArray

        for (int i = 0; i < wrappers.size(); i++) {
            JSONObject jGroup = new JSONObject();// /sub Object

            try {
                jGroup.put("routeNumber", wrappers.get(i).routeNumber);
                jGroup.put("length", wrappers.get(i).streetLength);
                jGroup.put("name", wrappers.get(i).streetName);
                jGroup.put("gender", wrappers.get(i).gender);
                jGroup.put("time", wrappers.get(i).timing);
                jArray.put(jGroup);
                jResult.put("itemDetail", jArray);




            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.i(TAG, String.valueOf(jResult));

        SendRequest sendRequest=new SendRequest(context);
        sendRequest.getSafestRoute(SERVER_URL, String.valueOf(jResult));
        return jResult;
    }

}