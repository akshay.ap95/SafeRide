package com.akshaypatel.saferide;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataParser {
    private static String TAG="===DataParser";
    /** Receives a JSONObject and returns a list of lists containing latitude and longitude */
    List<List<String>>stepDistances=new ArrayList<>();
    List<List<String>>legDistances=new ArrayList<>();
    List<List<HashMap<String,String>>> stepEndCoordinates=new ArrayList<>();
    public List<List<HashMap<String,String>>> parse(JSONObject jObject){

        List<List<HashMap<String, String>>> routes = new ArrayList<>() ;

        JSONArray jRoutes;
        JSONArray jLegs;
        JSONArray jSteps;
        JSONObject jLegDistnace;
        JSONObject jStepDistance;
        try {

            jRoutes = jObject.getJSONArray("routes");
            /** Traversing all routes */
            Log.i(TAG,"NUMBER OF ROUTES"+ jRoutes.length());

            for(int i=0;i<jRoutes.length();i++){
                jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList<>();
                List legs=new ArrayList<>();
                /** Traversing all legs */

              //  Log.i(TAG,"NUMBER OF LEGS FOR "+ i +" - "+ jLegs.length());
                List<HashMap<String, String> >stepEnds=new ArrayList<>();

                for(int j=0;j<jLegs.length();j++){
                    jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");


                    jLegDistnace = ( (JSONObject)jLegs.get(j)).getJSONObject("distance");
                    Log.i(TAG, String.valueOf(jLegDistnace));
                   // jLegDistnace.get();
                    legs.add(jLegDistnace.getString("text"));
                    /** Traversing all steps */
                    List step_distance=new ArrayList<>();
                    for(int k=0;k<jSteps.length();k++){
                        String polyline = "";
                        polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);
                        step_distance.add((String)((JSONObject)((JSONObject)jSteps.get(k)).get("distance")).get("text"));
                    //    Log.i(TAG, "==__ "+list.size());


                        // Use this Code to get the end cootdiantes of legs...i.e. uniuse streets

                        HashMap<String, String> hm1= new HashMap<>();
                        hm1.put("lat", Double.toString((list.get(0)).latitude) );
                        hm1.put("lng", Double.toString((list.get(list.size()-1)).longitude) );
                        stepEnds.add(hm1);


                        /** Traversing all points */
                        for(int l=0;l<list.size();l++){
                            HashMap<String, String> hm = new HashMap<>();
                            hm.put("lat", Double.toString((list.get(l)).latitude) );
                            hm.put("lng", Double.toString((list.get(l)).longitude) );
                       //     Log.i(TAG,"LAT ="+  Double.toString((list.get(l)).latitude) + " LONG:" + Double.toString((list.get(l)).longitude) );
                            path.add(hm);

                            //Points in step
                        }
                    }

                    stepEndCoordinates.add(stepEnds);
                    stepDistances.add(step_distance);
                 //  Log.i(TAG,"ALL STEPS"+ stepDistances);
                    routes.add(path);
                }
              //  Log.i(TAG," __"+stepEnds.size());
                legDistances.add(legs);
            }

              //  Log.i("===DATAPARSER", "ALL LEGS :"+String.valueOf(legDistances));
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }

        Log.i(TAG,"__"+stepEndCoordinates.size());
        return routes;
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    public List<List<String>> getLegDistances()
    {
        return legDistances;
    }


    public List<List<String>> getStepDistances()
    {
        return stepDistances;
    }

    public List<List<HashMap<String, String>>> getStepCoordinates()
    {

        int len=stepEndCoordinates.size();
        for(int i =0;i< len;i++)
        {
        List<HashMap<String,String>> a=stepEndCoordinates.get(i);
          // Log.i(TAG,"__"+a.size());
            for(int j=0;j<a.size();j++)
            {
                HashMap<String,String>xx=a.get(i);
           //    Log.i(TAG,"______CORDINATES"+a.get(i));
              //
            }
        }


        return stepEndCoordinates;
    }
}