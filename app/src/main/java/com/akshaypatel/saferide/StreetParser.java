package com.akshaypatel.saferide;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StreetParser {
    private static String TAG="===StreetParser";
    /** Receives a JSONObject and returns a list of lists containing latitude and longitude */

    String streetName=" ";
    String streetNumber=" ";

    public void parseStreet(JSONObject jObject){
     //  Log.i(TAG,"RECEIVED___RESPONSE FOR STREETNAME QUERY "+ jObject);
         try {
             JSONArray addressComp = jObject.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
             String formattedaddressComp = jObject.getJSONArray("results").getJSONObject(0).getString("formatted_address");

             /** Traversing all routes */
            // Log.i(TAG,"ADDRESS COMPONENTS"+ addressComp);
        //     Log.i(TAG,"FORMATTED ADDRESS"+ formattedaddressComp);

             for( int i=0;i<addressComp.length();i++)
             {
                 JSONArray typeKey=((JSONObject)addressComp.get(i)).getJSONArray("types");

                 String ss=typeKey.toString();

             //    Log.i(TAG,"FORMATTED ===___"+ ss);

           //     Log.i(TAG,"ADP COMPO"+typeKey.getClass().getName());
                 if(ss.equals("[\"street_number\"]") )
                 {
                     streetNumber=((JSONObject)addressComp.get(i)).getString("long_name");
                //    Log.i(TAG,"STREET NUMBER =="+ streetNumber);
                 }
                 else if(ss.equals("[\"route\"]"))
                 {
                     streetName=((JSONObject)addressComp.get(i)).getString("long_name");
                 //   Log.i(TAG,"STREET Name =="+ streetName);

                 }
                 else
                 {
                   //  Log.i(TAG,"STREET NONE MATCH"+ss);

                 }
             }

        } catch (JSONException e) {

             Log.i(TAG,"__________EXCEPTION");
            e.printStackTrace();
        }catch (Exception e){

             Log.i(TAG,"__________EXCEPTION");

         }


        return;
    }



    public String getStreetNumber()
    {
        return streetNumber;
    }


    public String getStreetName()
    {
        return streetName;
    }
}